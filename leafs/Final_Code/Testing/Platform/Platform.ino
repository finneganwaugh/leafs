#include <Servo.h>

Servo xServo;
Servo yServo;

int acceleroX = 0;
int acceleroY = 3;
int acceleroZ = 2;
int posX = 80;
int posY = 62;
int posZ = 0;
int storedz=0;

int calX = 0;
int calY = 0;
int calZ = 0;

void setup() {
  Serial.begin(9600);
  xServo.attach(10);
  yServo.attach(11);
  calibrate(); // Can we do this here
}

void calibrate() {
  xServo.write(posX);
  yServo.write(posY);

  for (int i=0; i < 3; i++) {  
    calX += analogRead(acceleroX);
    calY += analogRead(acceleroY);
    calZ += analogRead(acceleroZ);
    delay(1);
  }

  calX /= 3;
  calY /= 3;
  calZ /= 3;

  
}

void loop() {

  int x;
  int y;
  int z;
  
  for(int i = 0; i < 5; i++) {
    int rawX = analogRead(acceleroX); // Reads the x values straight from the accelerometer, using the variable names initiated above
    int rawY = analogRead(acceleroY); // Reads the y values straight from the accelerometer, using the variable names initiated above
    int rawZ = analogRead(acceleroZ); // Reads the z values straight from the accelerometer, using the variable names initiated above
  
    x += rawX - calX; //326
    y += rawY - calY; //347
    z += rawZ -calZ; //433
    delay(3);
  }

  x /= 5;
  y/= 5;
  z/= 5;
  


  
  //Serial.println("x: " + String(x) + ", y: " + String(y) + ", z: " + String(z));

  adjustY(y);
  adjustX(x);
 
  delay(50); 
}

float xConstant = 0.35;
void adjustX(int error) {
  //movePlatform(xServo, posX, 83 - error * xConstant);
  int newposX = posX + error * xConstant;
  xServo.write(newposX);
  
}

float yConstant = 0.35;
void adjustY(int error) {
  int newposY = posY - error * yConstant;
  yServo.write(newposY);
}

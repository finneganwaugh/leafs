//Tests that the wheels, and movement related functions are working as intended on the Slave Module

#include <Servo.h>

Servo leftWheel;
Servo rightWheel;

void setup() {
 leftWheel.attach(13);
 rightWheel.attach(12);

  
}

void loop() {
 go(200);
 delay(1000);
 turnonSpot(1, 200);
 delay(545);
 go(200);
 delay

 circleLeft();
 delay(5500);
 circleRight();
 delay(6700);
 go(200);
 

 
}


void go(int velocity){ // takes value in [-200, 200]
   leftWheel.writeMicroseconds(1490 + velocity * 1.15);
   rightWheel.writeMicroseconds(1490 - velocity);  
}

void turnonSpot(int direct, int v) {//direction = 1 for right, -1 for left
  leftWheel.writeMicroseconds(1490 + v * direct);
  rightWheel.writeMicroseconds(1490 + v * direct);
}

void circleLeft(){
  leftWheel.writeMicroseconds(1700);
  rightWheel.writeMicroseconds(1470);
}

void circleRight(){
  leftWheel.writeMicroseconds(1515);
  rightWheel.writeMicroseconds(1300);
}



#include <Servo.h>
Servo xServo;
Servo yServo;

int acceleroX = 0;
int acceleroY = 3;
int acceleroZ = 2;

int posX = 83;
int posY = 56;
int posZ = 0;

void setup() {
  Serial.begin(9600);
  xServo.attach(10);
  yServo.attach(11);
  calibrate(); // Can we do this here
}

void calibrate() {
  // calibrate code here
}

void loop() {
  int rawX = analogRead(acceleroX); // Reads the x values straight from the accelerometer, using the variable names initiated above
  int rawY = analogRead(acceleroY); // Reads the y values straight from the accelerometer, using the variable names initiated above
  int rawZ = analogRead(acceleroZ); // Reads the z values straight from the accelerometer, using the variable names initiated above

  //Serial.println("rawX: " + String(rawX) + ", rawY: " + String(rawY) + ", rawZ: " + String(rawZ)); // prints above variables to serial monitor, ln creates a new line
/*
  float x = convertGForce(rawX, -0.07);
  float y = convertGForce(rawY,-0.26);
  float z = convertGForce(rawZ, -0.06);
  */
 
  int x = rawX - 326;
  int y = rawY - 347;
  int z = rawZ -434;

  Serial.println("x: " + String(x) + ", y: " + String(y) + ", z: " + String(z));

  if(z < -10 || z > 10) {
    adjustY(y);
    adjustX(x);
  }
  
  delay(50); 
}

float xConstant = 0.25;
void adjustX(int error) {
  movePlatform(xServo, posX, 83 - error * xConstant);
  posX = 83 - error * xConstant;
}

float yConstant = 0.25;
void adjustY(int error) {
  movePlatform(yServo, posY, 56 - error * yConstant);
  posY = 56 - error * yConstant;
}

void movePlatform(Servo servo, int oldPos, int newPos) {
  for(int i = oldPos; abs(i - newPos) > 1; i += int((oldPos - newPos)/5)) {
    Serial.println(abs(i - newPos));
    servo.write(i);
  }
}
/*
float convertVoltage(int x) {
  return x /1024.0 * 5.0;
}

float convertGForce(int x, float error) {
  float volts = convertVoltage(x);
  return ((volts - 1.55)/0.8)*1 + error;
}
*/




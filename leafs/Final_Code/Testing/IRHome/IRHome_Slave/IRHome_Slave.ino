//Slave component of the IR return home test cases

//INITIALISATIONS
//Servo-related Initialisations
#include <Servo.h>
Servo leftWheel;
int leftServoPin = 13;
Servo rightWheel;
int rightServoPin = 12;

//IR initialisations
int leftIRPin = 9;
int rightIRPin = 8;


void setup() {
    irSetup();
    servoSetup();
    Serial.begin(9600); 
    
}



void loop() {
  goHome();
  delay(50);
    
}

void irSetup() {
  //initialises pins related to IR
  
  pinMode(leftIRPin, INPUT);
  pinMode(rightIRPin, INPUT);
  
}

//IR RETURN HOME

int direct = 1;

void goHome() {
  //This function directs the slave back to home base.
  //It turns in a circle, and then when it detects IR, it moves in that direction.
  
  boolean leftIR = digitalRead(leftIRPin);
  boolean rightIR = digitalRead(rightIRPin);


  Serial.println("left: " + String(leftIR) + "right:" + String(rightIR)); //Debugging
  if(!leftIR && !rightIR) {
    turnonSpot(direct, 100);
  }
  else if(!rightIR) {
    circleLeft();
    direct = -1;
  }
  else if(!leftIR) {
    circleRight();
    direct = 1;
  }
  else{
    go(200);
  }

  delay(3);
}
 

//MOVEMENT FUNCTIONS
void go(int velocity){ 
   //Moves the robot forwards (or backwards) at the given speed
   // takes value in [-200, 200]
   
   leftWheel.writeMicroseconds(1490 + velocity * 1.15); //a constant that evens out the left and right wheels
   rightWheel.writeMicroseconds(1490 - velocity);  
}

void turnonSpot(int direct, int v) {
  // Turns the robot on the spot in given direction at speed v
  // int direct = 1 for right, -1 for left
  
  leftWheel.writeMicroseconds(1490 + v * direct);
  rightWheel.writeMicroseconds(1490 + v * direct);
}

void circleLeft(){
  //Turns / Veers the robot left
  
  leftWheel.writeMicroseconds(1700);
  rightWheel.writeMicroseconds(1460);
}

void circleRight(){
  //Turns / Veers the robot right
  
  leftWheel.writeMicroseconds(1530);
  rightWheel.writeMicroseconds(1300);
}

void servoSetup() {
    //set-up servos
  
    leftWheel.attach(leftServoPin);
    rightWheel.attach(rightServoPin);
}


//IR Variables
int irLeft = 9;
int irRight = 10;

void setup() {

  Serial.begin(9600);
  irSetup();
  tone(irLeft, 38000);

}

void loop() {
  delay(5);
}

void irSetup() {
  //Carries out setup for IR functions.
  
  pinMode(irLeft, OUTPUT);
  pinMode(irRight, OUTPUT);
}

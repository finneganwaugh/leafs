/*  This file contains the code for the Master component of the major project of ELEC 1601.
 *  
 *  Sections relating to BlueTooth functionality (specifically the intialisation beneath //Bluetooth-related Variables, code in 
 *  void bluetoothSetup();, and setupBluetoothConnection(); were sourced from www.seeedstudio.com/
 *  
 *  All other code was written by the LEAFS ELEC1601 Team, (F09 -12), comprised of Finnegan Waugh, Lewis Watts, Sean Dixon and Alessandra Lee.
 *  
 **********************************************************************
 *
 *  The Master Component of the code is simpler. After initialising and connecting bluetooth it performs two main functions in its loop.
 *  1. it checks if any buttons have been pressed. If so, sends relevant code to slave (usually movement related). The buttons also change
 *  the LEDs on the Master
 *  2. it checks the 'slave' serial for the state of the light sensor. depending on that status it decides to display platform containing item
 *  or platform not containing item
 */ 

//VARIABLE INITIALISATION
//Bluetooth-related Variables
#include <SoftwareSerial.h> // Software Serial Port
#define RxD 7
#define TxD 6
#define DEBUG_ENABLED  1
String retSymb = "+RTINQ="; // start symble when there's any return
int BTNum = 10; //MUST BE CHANGED EACH TIME - Refers to M-## number of bluetooth shield
String slaveName = ";Slave" + String(BTNum);
int nameIndex = 0;
int addrIndex = 0;
String recvBuf;
String slaveAddr;
String connectCmd = "\r\n+CONN=";
SoftwareSerial blueToothSerial(RxD,TxD);

//IR Variables
int irLeft = 8;
int irRight = 9;

//Button / Light set-up
int leftD = 2;
int rightD = 4;
int upD = 5;
int downD = 3;
int homeButton;
int movingLED = 13;
int stoppingLED = 12;
int fullLED = 11;
int emptyLED = 10;;

void setup() {
    Serial.begin(9600);
    irSetup();
    bluetoothSetup();
    buttonSetup();
}

char recvChar;
char sendChar;
char prevSendChar;


void loop() {
    if(blueToothSerial.available()) {         //check if there's any data sent from the remote bluetooth shield
        recvChar = blueToothSerial.read();
        //Serial.print(recvChar);   //DEBUG
    }

    switch(recvChar) {

      case 'l': //'l' is light
        digitalWrite(fullLED, HIGH);
        digitalWrite(emptyLED, LOW);
        break;

      case 'k': //'k' is dark
        digitalWrite(emptyLED, HIGH);
        digitalWrite(fullLED, LOW);
        //sendCommand('h') //return home if no object on plattform
        break;
      
      default:
        //
        break;
      
    }

    //if(!digitalRead(homeButton)) {
    // sendCommand('h');
   // }

    dPadMovement();
    delay(5);
}

void dPadMovement() {
  //Reads from the d-pad of buttons, prints appropriately to Serial, and changes LEDs

  boolean left = digitalRead(leftD);
  boolean right = digitalRead(rightD);
  boolean up = digitalRead(upD);
  boolean down = digitalRead(downD);

  if(!up) {
   sendCommand('w');
    digitalWrite(movingLED, HIGH);
    digitalWrite(stoppingLED, LOW);
  } else if(!down) {
   sendCommand('s');
    digitalWrite(movingLED, LOW);
    digitalWrite(stoppingLED, HIGH);
  } else if(!left) {
   sendCommand('a');
    digitalWrite(movingLED, HIGH);
    digitalWrite(stoppingLED, LOW);
  } else if(!right) {
   sendCommand('d');
    digitalWrite(movingLED, HIGH);
    digitalWrite(stoppingLED, LOW);
  }
}

void sendCommand(char c) {
  if(c != prevSendChar) {
    Serial.print(c);
    prevSendChar = c;
  }
}

void bluetoothSetup() {
   //Executes the necessary setup for Bluetooth Communication. Can be found at www.seeedstudio.com/
   
   pinMode(RxD, INPUT);
   pinMode(TxD, OUTPUT);
   setupBlueToothConnection();
            //wait 1s and flush the serial buffer
    delay(1000);
    Serial.flush();
    blueToothSerial.flush();
}

void irSetup() {
  //Carries out setup for IR functions.
  
  pinMode(irLeft, OUTPUT);
  pinMode(irRight, OUTPUT);
  tone(irLeft, 38000);
}

void buttonSetup() {
  //Initialises remainign pins (buttons & lights)
  
  pinMode(leftD, INPUT);
  pinMode(rightD, INPUT);
  pinMode(upD, INPUT);
  pinMode(downD, INPUT);
  pinMode(homeButton, INPUT);
  pinMode(movingLED, OUTPUT);
  pinMode(stoppingLED, OUTPUT);
  pinMode(fullLED, OUTPUT);
  pinMode(emptyLED, OUTPUT);
}

void setupBlueToothConnection() {
    //Completes BT related setup. Can be found at www.seeedstudio.com/
    
    blueToothSerial.begin(38400);                               // Set BluetoothBee BaudRate to default baud rate 38400
    blueToothSerial.print("\r\n+STWMOD=1\r\n");                 // set the bluetooth work in master mode
    blueToothSerial.print("\r\n+STNA=Master" + String(BTNum) + "\r\n"); // set the bluetooth name as
    blueToothSerial.print("\r\n+STAUTO=0\r\n");                 // Auto-connection is forbidden here
    delay(2000);                                                // This delay is required.
    blueToothSerial.flush();
    blueToothSerial.print("\r\n+INQ=1\r\n");                    //make the master inquire
    Serial.println("Master is inquiring!");
    delay(2000); // This delay is required.
    //find the target slave
    char recvChar;
    while(1){
        if(blueToothSerial.available()) {
            recvChar = blueToothSerial.read();
            recvBuf += recvChar;
            nameIndex = recvBuf.indexOf(slaveName);             //get the position of slave name
                                                                //nameIndex -= 1;
                                                                //decrease the ';' in front of the slave name, to get the position of the end of the slave address
            if ( nameIndex != -1 ) {
                //Serial.print(recvBuf);
                addrIndex = (recvBuf.indexOf(retSymb,(nameIndex - retSymb.length()- 18) ) + retSymb.length());//get the start position of slave address
                slaveAddr = recvBuf.substring(addrIndex, nameIndex);//get the string of slave address
                break;
            }
        }
    }
    //form the full connection command
    connectCmd += slaveAddr;
    connectCmd += "\r\n";
    int connectOK = 0;
    Serial.print("Connecting to slave:");
    Serial.print(slaveAddr);
    Serial.println(slaveName);
    //connecting the slave till they are connected
    do {
        blueToothSerial.print(connectCmd);//send connection command
        recvBuf = "";
        while(1) {
            if(blueToothSerial.available()){
                recvChar = blueToothSerial.read();
                recvBuf += recvChar;
                if(recvBuf.indexOf("CONNECT:OK") != -1)
                {
                    connectOK = 1;
                    Serial.println("Connected!");
                    blueToothSerial.print("Connected!");
                    break;
                }
                else if(recvBuf.indexOf("CONNECT:FAIL") != -1)
                {
                    Serial.println("Connect again!");
                    break;
                }
            }
        }
    }while(0 == connectOK);
}

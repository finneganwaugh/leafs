/*  This file contains the code for the Slave component of the major project of ELEC 1601.
 *  
 *  Sections relating to BlueTooth functionality (specifically the intialisation beneath //Bluetooth-related Variables, code in 
 *  void bluetoothSetup();, and setupBluetoothConnection(); were sourced from www.seeedstudio.com/
 *  
 *  All other code was written by the LEAFS ELEC1601 Team, (F09 -12), comprised of Finnegan Waugh, Lewis Watts, Sean Dixon and Alessandra Lee.
 *  
 **********************************************************************
 *
 *  The Slave Component performs a number of functions. After initialising and connecting bluetooth and calibrating acceleroemeter it has a 
 *  number of operations performed each loop:
 *  1. Reads from the Master Terminal to check for instructions. Acts according to instructions (primarily movement).
 *  2. Adjusts the platform to lie flat by reading from the accelerometer.
 *  3. Reads from the light-sensor and prints to its own terminal if there's a change.
 */ 

//VARIABLE INITIALISATION
// Bluetooth setup
#include <SoftwareSerial.h>   //Software Serial Port
#define RxD 7
#define TxD 6
#define DEBUG_ENABLED  1
SoftwareSerial blueToothSerial(RxD, TxD);
int BTNUM = 10; //MUST BE CHANGED EACH TIME
char lastChar;

//Servo-related Initialisations
#include <Servo.h>
Servo leftWheel;
int leftServoPin = 13;
Servo rightWheel;
int rightServoPin = 12;
Servo xServo;
int xServoPin = 10;
Servo yServo;
int yServoPin = 11;


//IR initialisations
int leftIRPin = 9;
int rightIRPin = 8;

//Accelerometer Initialisations
int acceleroX = 0;
int acceleroY = 3;
int acceleroZ = 2;
int posX = 89;
int posY = 63;
int posZ = 0;

int calX = 0;
int calY = 0;
int calZ = 0;


//Sensor Initialisations
int lightPin = 4;

void setup() {
  Serial.begin(9600); 
  servoSetup();
  irSetup();
  bluetoothSetup();

}


char control = '.';

void loop() {  //MAIN LOOP
    //go(25);  

  if (blueToothSerial.available()) {
    control = blueToothSerial.read(); // takes inputs from Master
    //Serial.println(control); //DEBUG
  }

  switch (control) { //Runs differing commands depending on the Master's input

    case 'h': //'h' is the command to return to master module
      goHome();
      break;

    case 'w': //'w' is the command to move forward
      go(75);
      break;

    case 'a': // 'a' is the command to turn left
      turnOnSpot(-1, 50);
      break;

    case 's': // 's' is the command to stop
      go(0);
      break;

    case 'd': // 'd' is the command to turn right
      turnOnSpot(1, 50);
      break;

    default:
      go(0);
      break;
      
  }
  adjustPlatform(); //keep platform stable

  //Check for light
  int light = analogRead(lightPin);
  if(light < 900) { //if there is an object (low light level) , send input
    if(lastChar != 'k') {
      lastChar = 'k';
      Serial.print('k'); // k = dark / food
    }
  } else {
    if(lastChar!= 'l') {
      lastChar = 'l';
      Serial.print('l');
    }
  }
  delay(5);
}

//STABILISING PLATFORM

void adjustPlatform() {
  //Reads from the accelerometer and then adjusts the platform according to the changes
  
  int x;
  int y;
  int z;
  
  for(int i = 0; i < 10; i++) {
    int rawX = analogRead(acceleroX); // Reads the x values straight from the accelerometer, using the variable names initiated above
    int rawY = analogRead(acceleroY); // Reads the y values straight from the accelerometer, using the variable names initiated above
    int rawZ = analogRead(acceleroZ); // Reads the z values straight from the accelerometer, using the variable names initiated above
  
    x += rawX - calX; //326
    y += rawY - calY; //347
    z += rawZ - calZ; //433
    delay(2);
  }

  x /= 10;
  y/= 10;
  z/= 10;
  
  //Serial.println("x: " + String(x) + ", y: " + String(y) + ", z: " + String(z));

  adjustY(y);
  adjustX(x);
 
}

float xConstant = 0.35;
void adjustX(int desired_x) {
  //adjusts the xServo to the given position
  
  int newposX = posX + desired_x * xConstant; //the xConstant converts the change in acceleration to a corresponding position
  
  //if (newposX )
  xServo.write(newposX);
}

float yConstant = 0.35;
void adjustY(int desired_y) {
  
  int newposY = posY - desired_y * yConstant;
  yServo.write(newposY);
}


//IR RETURN HOME

int direct = 1;

void goHome() {
  //This function directs the slave back to home base.
  //It turns in a circle, and then when it detects IR, it moves in that direction.
  
  boolean leftIR = digitalRead(leftIRPin);
  boolean rightIR = digitalRead(rightIRPin);

  //Serial.print("left: " + String(leftIR) + "right:" + String(rightIR)); //Debugging
  if(!leftIR && !rightIR) {
    turnonSpot(direct, 25);
  }
  else if(!rightIR) {
    circleLeft();
    direct = -1;
  }
  else if(!leftIR) {
    circleRight();
    direct = 1;
  }
  else{
    go(200);
  }
}
 

//MOVEMENT FUNCTIONS
void go(int velocity){ 
   //Moves the robot forwards (or backwards) at the given speed
   // takes value in [-200, 200]
   
   leftWheel.writeMicroseconds(1490 + velocity * 1.15); //a constant that evens out the left and right wheels
   rightWheel.writeMicroseconds(1490 - velocity);  
}

void turnonSpot(int direct, int v) {
  // Turns the robot on the spot in given direction at speed v
  // int direct = 1 for right, -1 for left
  
  leftWheel.writeMicroseconds(1490 + v * direct);
  rightWheel.writeMicroseconds(1490 + v * direct);
}

void circleLeft(){
  //Turns / Veers the robot left
  
  leftWheel.writeMicroseconds(1700);
  rightWheel.writeMicroseconds(1460);
}

void circleRight(){
  //Turns / Veers the robot right
  
  leftWheel.writeMicroseconds(1530);
  rightWheel.writeMicroseconds(1300);
}


//SET-UP FUNCTIONS
void irSetup() {
  //initialises pins related to IR
  
  pinMode(leftIRPin, INPUT);
  pinMode(rightIRPin, INPUT);
  
}


void bluetoothSetup() {
    //Executes Bluetooth Related setup
  
    pinMode(RxD, INPUT);
    pinMode(TxD, OUTPUT);
    setupBlueToothConnection();
}

void servoSetup() {
    //set-up servos
  
    leftWheel.attach(leftServoPin);
    rightWheel.attach(rightServoPin);
    xServo.attach(xServoPin);
    yServo.attach(yServoPin);
    calibrate();
    
}

void calibrate() {
  //calibrate accelerometer error values

  xServo.write(posX);
  yServo.write(posY);

  for (int i=0; i < 5; i++) {  
    calX += analogRead(acceleroX);
    calY += analogRead(acceleroY);
    calZ += analogRead(acceleroZ);
    delay(5);
  }

  calX /= 5;
  calY /= 5;
  calZ /= 5;
}


void setupBlueToothConnection() {
    //connects with Master BT Module
    
    blueToothSerial.begin(38400);                           // Set BluetoothBee BaudRate to default baud rate 38400
    blueToothSerial.print("\r\n+STWMOD=0\r\n");             // set the bluetooth work in slave mode
    blueToothSerial.print("\r\n+STNA=Slave" + String(BTNUM) + "+\r\n"); // set the bluetooth name as "SeeedBTSlave"
    blueToothSerial.print("\r\n+STOAUT=1\r\n");             // Permit Paired device to connect me
    blueToothSerial.print("\r\n+STAUTO=0\r\n");             // Auto-connection should be forbidden here
    delay(2000);                                            // This delay is required.
    blueToothSerial.print("\r\n+INQ=1\r\n");                // make the slave bluetooth inquirable
    Serial.println("The slave bluetooth is inquirable!");
    delay(2000);                                            // This delay is required.

    blueToothSerial.flush();
}
